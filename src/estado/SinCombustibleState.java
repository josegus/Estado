package estado;

public class SinCombustibleState implements State {
    
    private Vehiculo vehiculo;    
    
    public SinCombustibleState(Vehiculo v) {
        vehiculo = v;
    }
    
    @Override
    public void acelerar() {
        // System.out.println("Error: El vehiculo se encuentra sin combustible");
        vehiculo.setDescripcion("Error: El vehiculo se encuentra sin combustible");
    }
    
    @Override
    public void frenar() {
        // System.out.println("Error: El vehiculo se encuentra sin combustible");
        vehiculo.setDescripcion("Error: El vehiculo se encuentra sin combustible");
    }
    
    @Override
    public void contacto() {
        // System.out.println("Error: El vehiculo se encuentra sin combustible");
        vehiculo.setDescripcion("Error: El vehiculo se encuentra sin combustible");
    }
}
