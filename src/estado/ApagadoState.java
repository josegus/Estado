package estado;

public class ApagadoState implements State {
    
    // Referencia a la clase de contexto
    private Vehiculo vehiculo;   
    
    public ApagadoState(Vehiculo v) {
        vehiculo = v;
    }
    
    @Override
    public void acelerar() {
        // System.out.println("Error: El vehículo está apagado. Efectúe el contacto para iniciar");
        vehiculo.setDescripcion("Error: El vehículo está apagado. Efectúe el contacto para iniciar");
    }
    
    @Override
    public void frenar() {
        // System.out.println("Error: El vehículo está apagado. Efectúe el contacto para iniciar");
        vehiculo.setDescripcion("Error: El vehículo está apagado. Efectúe el contacto para iniciar");
    }
    
    @Override
    public void contacto() {
        // Comprobar que el vehiculo tiene combustible
        if (vehiculo.getCombustibleActual() > 0) {
            vehiculo.setEstado(new ParadoState(vehiculo));            
            vehiculo.setVelocidadActual(0);
            
            // System.out.println("El vehículo se encuentra PARADO");
            vehiculo.setDescripcion("El vehículo se encuentra PARADO");
        } else {
            // El vehículo no tiene combustible, no puede arrancar
            vehiculo.setEstado(new SinCombustibleState(vehiculo));
            
            // System.out.println("El vehículo se encuentra SIN COMBUSTIBLE");
            vehiculo.setDescripcion("El vehículo se encuentra SIN COMBUSTIBLE");
        }
    }
}