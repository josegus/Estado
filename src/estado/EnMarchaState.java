package estado;

public class EnMarchaState implements State {
    
    private static int VELOCIDAD_MAXIMA = 160;
    private Vehiculo vehiculo;   
    
    public EnMarchaState(Vehiculo v) {
        vehiculo = v;
    }
    
    @Override
    public void acelerar() {
        if (vehiculo.getCombustibleActual() > 0) {
            // Aumentar la velocidad, permaniciendo en el mismo estado
            if (vehiculo.getVelocidadActual() >= VELOCIDAD_MAXIMA) {                
                vehiculo.ModificarCombustible(-10);
                
                // System.out.println("Error: El vehiculo ha alcanzado su velocidad máxima");
                vehiculo.setDescripcion("Error: El vehiculo ha alcanzado su velocidad máxima");
            } else {
                // Se mantiene el estado actual
                vehiculo.setDescripcion("El vehículo ha acelerado.. se encuentra EN MARCHA");
                vehiculo.ModificarVelocidad(10);
                vehiculo.ModificarCombustible(-10);
            }
        } else {
            // El vehiculo no tiene combustible
            vehiculo.setEstado(new SinCombustibleState(vehiculo));
            
            // System.out.println("El vehículo se ha quedado sin combustible");
            vehiculo.setDescripcion("El vehículo se ha quedado sin combustible");
        }
    }
    
    @Override
    public void frenar() {
        // Reducir velocidad. Si esta llega a 0, se debe cambiar la velocidad
        vehiculo.ModificarVelocidad(-10);
        if (vehiculo.getVelocidadActual() <= 0) {
            vehiculo.setEstado(new ParadoState(vehiculo));
            
            // System.out.println("El vehículo se encuentra ahora PARADO");
            vehiculo.setDescripcion("El vehículo se encuentra ahora PARADO");
        } else {
            // Se mantiene el estado actual, pero el vehículo ha reducido su velocidad
            vehiculo.setDescripcion("El vehículo ha frenado.. pero aún se encuentra EN MARCHA");
        }
    }
    
    @Override
    public void contacto() {
        // No se puede detener el vehiculo en marcha        
        // System.out.println("--Error: No se puede cortar el contacto en marcha!");       
        vehiculo.setDescripcion("Error: No se puede cortar el contacto en marcha!");
    }
}
