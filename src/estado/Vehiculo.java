package estado;

public class Vehiculo {
    
    // Atributos 
    private State estado;                 // Estado actual del vehiculo (apagado, parado, en marcha, sin combustible)
    private int velocidadActual = 0;      // Velocidad actual del vehiculo
    private int combustibleActual = 0;    // Cantidad de combustible restante 
    private String descripcion = "";      // Descripción del estado en el que se encuentra el vehículo
    
    // Asigna o recupera el estado del vehiculo
    public State getEstado() {        
        return estado;
    }

    public void setEstado(State nuevoEstado) {
        estado = nuevoEstado;
    }
 
    // Asigna o recupera la velocidad actual del vehiculo
    public int getVelocidadActual() {
        return velocidadActual;
    }

    public void setVelocidadActual(int nuevaVelocidad) {
        velocidadActual = nuevaVelocidad;
    }
 
    // Obtiene la cantidad de combustible actual
    public int getCombustibleActual() {
        return combustibleActual;
    }
    
    // Asigna o recupera la descripción del vehículo
    public String getDescripcion() {
        return descripcion;
    }
    
    public void setDescripcion(String nuevaDescripcion) {
        descripcion = nuevaDescripcion;
    }
 
    // Constructores 
    // El constructor inserta el combustible del que dispondra el vehiculo
    public Vehiculo(int combustible) {
        this.combustibleActual = combustible;
        
        //Indicar un estado inicial (Apagado)
        estado = new ApagadoState(this);
    }
 
    // Metodos relacionados con los estados 
    // Los metodos del contexto invocaran el metodo de la interfaz State, delegando las operaciones
    // dependientes del estado en las clases que los implementen.
    public void Acelerar() {
        estado.acelerar();       
    }
 
    public void Frenar() {
        estado.frenar();
    }
 
    public void Contacto() {
        estado.contacto();
    } 
  
    // Otros métodos
    public void ModificarVelocidad(int kmh) {
        velocidadActual += kmh;
    }
 
    public void ModificarCombustible(int litros) {
        combustibleActual += litros;
    }  
}
