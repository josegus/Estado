package estado;

public class ParadoState implements State {
    
    private Vehiculo vehiculo;           
    
    public ParadoState(Vehiculo v) {
        vehiculo = v;
    }
    
    @Override
    public void acelerar() {
        if (vehiculo.getCombustibleActual() > 0) {
            vehiculo.setEstado(new EnMarchaState(vehiculo));            
            vehiculo.ModificarVelocidad(10);
            vehiculo.ModificarCombustible(-10);
            
            // System.out.println("El vehículo se encuentra EN MARCHA");
            vehiculo.setDescripcion("El vehículo se encuentra EN MARCHA");
        } else {
            vehiculo.setEstado(new SinCombustibleState(vehiculo));
            
            // System.out.println("El vehículo se encuentra SIN COMBUSTIBLE");
            vehiculo.setDescripcion("El vehículo se encuentra SIN COMBUSTIBLE");
        }
    }
    
    @Override
    public void frenar() {
        // System.out.println("Error: El vehículo ya se encuentra PARADO");
        vehiculo.setDescripcion("Error: El vehículo ya se encuentra PARADO");
    }
    
    @Override
    public void contacto() {
        vehiculo.setEstado(new ApagadoState(vehiculo));       
        // System.out.println("El vehículo ahora se encuentra APAGADO");
        vehiculo.setDescripcion("El vehículo ahora se encuentra APAGADO");
    }
}
